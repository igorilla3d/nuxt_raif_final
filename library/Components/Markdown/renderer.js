export default function mdFactory() {
    const marked = require('marked');

    const renderer = new marked.Renderer();

    // Use divs instead of actual heading elements (SEO I guess)
    renderer.heading = (text, level) => `<div class="h${level}">${text}</div>`;

    marked.setOptions({
        // TODO see https://marked.js.org/#/USING_ADVANCED.md#options
        headerIds: false,
        renderer
    });

    return marked;
}
