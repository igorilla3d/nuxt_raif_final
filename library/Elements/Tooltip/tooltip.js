import Popper from 'popper.js';

const createTooltip = (el, pops) => {
    let _tooltipWrap = el.querySelectorAll(`.tooltipWrap`);

    if (_tooltipWrap.length) {
        for (let i = 0; i < _tooltipWrap.length; i++) {
            let _wrap = _tooltipWrap[i];
            let _tooltip = _wrap.querySelector('.tooltipBody');

            let _pop = new Popper(_wrap, _tooltip, {
                placement: 'bottom-start',
                eventsEnabled: false,
                // removeOnDestroy: true,
                modifiers: {
                    offset: { enabled: false },
                    preventOverflow: {
                        priority: ['left', 'right'],
                        // boundariesElement: 'window'
                    },
                    keepTogether: { enabled: false },
                    arrow: { enabled: false },
                    flip: { enabled: false },
                    inner: { enabled: false },
                    hide: { enabled: false }
                },
            });

            pops.push(_pop);

            setTimeout(() => {
                _pop.scheduleUpdate();
            }, 1000);
        }
    }
};

export { createTooltip };

const destroyTooltip = (pops) => {
    pops.forEach(pop => {
        pop.destroy();
    });
};

export { destroyTooltip };

const updateTooltip = (pops) => {
    pops.forEach(pop => {
        pop.scheduleUpdate();
    });
};

export { updateTooltip };
