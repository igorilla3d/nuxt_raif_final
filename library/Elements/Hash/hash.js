const navigateToHash = (slides, slider) => {
    let _hash = window.location.hash.replace('#', '');
    if (_hash) {
        let _index = slides.findIndex(slide => slide.tag === _hash);
        if (_index && _index !== -1) {
            slider.slideToIndex(_index);
            // window.history.pushState('', '', `${window.location.pathname}#${_hash}`);
        }
    }
};

export { navigateToHash };