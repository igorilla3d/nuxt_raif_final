const OFF = 0;
const WARN = 1;
const ERROR = 2;

module.exports = {
  root: true,
  env: {
    browser: true,
    node: true,
  },
  extends: [
    'plugin:vue/base',
    'plugin:vue/essential',
    'plugin:vue/recommended',
    'plugin:vue/strongly-recommended',
    '@vue/typescript',
    // '@nuxtjs',
    // 'plugin:nuxt/recommended'
  ],
  rules: {
    'indent': OFF,
    'vue/html-indent': ['error', 4],
    'vue/max-attributes-per-line': OFF,
    'vue/multiline-html-element-content-newline': OFF,
    'vue/singleline-html-element-content-newline': OFF,
    "vue/no-v-html": "off",
    // 'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    // 'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
  },
  parserOptions: {
    parser: '@typescript-eslint/parser',
  },
};
