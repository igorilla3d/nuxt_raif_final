const path = require('path');

module.exports = ({ config, mode }) => {
    config.resolve.extensions.push('.ts', '.tsx', '.vue', '.css', '.less', '.scss', '.sass', '.html', '.json');

    config.resolve.alias = Object.assign(config.resolve.alias, {
        '~': path.dirname(path.resolve(__dirname)),
        '@': path.dirname(path.resolve(__dirname))
    });

    config.module.rules.push(
        {
            test: /\.scss$/,
            use: [
                'vue-style-loader',
                'css-loader',
                {
                    loader: 'sass-loader',
                    options: {
                        data: '@import "~/assets/styles/variables";',
                    }
                }
            ],
        }, {
            test: /\.ts$/,
            exclude: /node_modules/,
            use: [
                {
                    loader: 'ts-loader',
                    options: {
                        appendTsSuffixTo: [/\.vue$/],
                        transpileOnly: true
                    },
                }
            ]
        }
    );

    return config;
};
