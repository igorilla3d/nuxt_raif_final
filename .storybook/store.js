import Vue from 'vue'
import Vuex from 'vuex'
import { state, actions, mutations } from '@/store/index'

Vue.use(Vuex);

let store = new Vuex.Store({
    state,
    actions,
    mutations
});

export default store;
