import { addParameters, configure } from '@storybook/vue';
import { themes } from '@storybook/theming';
import Vue from 'vue';
import '~/assets/styles/globals.scss';
import '~/assets/styles/grid.scss';

Vue.component('nuxt-link', {
    props:   ['to'],
    methods: {
        log() {
            console.log(`link target ${this.to}`);
        },
    },
    template: '<a href="#" @click.prevent="log()"><slot>NuxtLink</slot></a>',
});

Vue.component('no-ssr', {
    template: '<div><slot></slot></div>',
});

addParameters({
    options: {
        showPanel: false,
        isToolshown: false,
        // theme: undefined,
        theme: themes.dark
    }
});

const loadStories = () => {
    require('../stories');
};

configure(loadStories, module);
