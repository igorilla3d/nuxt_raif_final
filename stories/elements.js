import { storiesOf } from "@storybook/vue";
import { boolean, text, number, withKnobs } from "@storybook/addon-knobs";

import TextLinkV1 from '~/library/Elements/Links/TextLinkV1.vue';
import TextLinkWithArrowV1 from '~/library/Elements/Links/TextLinkWithArrowV1.vue';
import TextLinkWithIconV1 from '~/library/Elements/Links/TextLinkWithIconV1.vue';
import IconLinkV1 from '~/library/Elements/Links/IconLinkV1.vue';
import ButtonV1 from '~/library/Elements/Buttons/ButtonV1.vue';
import ListV1 from '~/library/Elements/Lists/ListV1.vue';
import IconsNavV1 from '~/library/Elements/Navigations/IconsNavV1.vue';
import TextNavWithIconsV1 from '~/library/Elements/Navigations/TextNavWithIconsV1.vue';
import StarsV1 from '~/library/Elements/Stars/StarsV1.vue';
import BurgerV1 from '~/library/Elements/Icons/BurgerV1.vue';
import BackToTopV1 from '~/library/Elements/Icons/BackToTopV1.vue';
import PremiumDirectLinks from '~/library/Elements/PremiumDirectSwitch/PremiumDirectSwitchV1.vue';
import SwitchV1 from '~/library/Elements/Switches/SwitchV1.vue';
import RangeSliderV1 from '~/library/Elements/RangeSliders/RangeSliderV1.vue';
import RangeSliderWithInputV1 from '~/library/Elements/RangeSliders/RangeSliderWithInputV1.vue';
import TableV1 from '~/library/Elements/Tables/TableV1.vue';
import ImageWithDownloadV1 from '~/library/Elements/Images/ImageWithDownloadV1.vue';
import AccordionV1 from '~/library/Elements/Accordions/AccordionV1.vue';
import Card from '~/library/Elements/Cards/NewsCardV1.vue';

storiesOf('Elements/Links', module)
.addDecorator(withKnobs)
.add('Footer link', () => ({
    render: h => h(TextLinkV1, { class: 'link__footer' })
}))
.add('Link with text and arrow', () => {
    return {
        props: {
            name: { default: text('Name', 'Узнать больше') },
            backlink: { default: boolean('Backlink', false) }
        },
        render(h) { return h(TextLinkWithArrowV1, { props: { name: this.name, backlink: this.backlink } }); }
    };
})
.add('Backlink with text and arrow', () => {
    return {
        props: {
            name: { default: text('Name', 'Назад к акциям') },
            backlink: { default: boolean('Backlink', true) }
        },
        render(h) { return h(TextLinkWithArrowV1, { props: { name: this.name, backlink: this.backlink } }); }
    };
})
.add('Link with icon and text', () => ({
    render: h => h(TextLinkWithIconV1, {
        class: 'icon__top',
        props: {
            name: 'Инвестиционное и брокерское <span>обслуживание</span>',
            classes: 'icon__advantage1',
        }
    })
}))
.add('Social icon link', () => ({
    render: h => h(IconLinkV1, { class: 'social__icon social__viber' })
}))
.add('Store icon link', () => ({
    render: h => h(IconLinkV1, { class: 'store__icon store__apple' })
}));

storiesOf('Elements/Buttons', module)
.addDecorator(withKnobs)
.add('Button', () => {
    return {
        props: { // Must use prop defaults for knobs to work
            name: { default: text('Label', 'Кнопка') },
            invert: { default: boolean('Invert', false) }
        },
        // Do not use arrow functions to allow proper context binding
        render(h) { return h(ButtonV1, { props: { name: this.name, inverted: this.invert } }); }
    };
})
.add('Button inverted', () => {
    return {
        props: {
            name: { default: text('Label', 'Кнопка') },
            invert: { default: boolean('Invert', true) }
        },
        render(h) { return h(ButtonV1, { props: { name: this.name, inverted: this.invert } }); }
    };
});

storiesOf('Elements/Lists', module)
.addDecorator(withKnobs)
.add('Unordered list', () => {
    return {
        props: {
            ordered: { default: boolean('Ordered', false) }
        },
        render(h) { return h(ListV1, { props: { ordered: this.ordered } }); }
    };
})
.add('Ordered list', () => {
    return {
        props: {
            ordered: { default: boolean('Ordered', true) }
        },
        render(h) { return h(ListV1, { props: { ordered: this.ordered } }); }
    };
})
.add('Unordered list with nested ordered lists', () => {
    return {
        render(h) { return h(ListV1, { props: {
                ordered: false,
                items: [
                    {
                        text: 'Для всех регионов:',
                        listItems: [
                            {
                                text: 'не ранее 2000 года, этажностью не менее 3 этажей вне зависимости от материала стен.'
                            }
                        ],
                        ordered: true,
                    }, {
                        text: 'Для Москвы и Московской области:',
                        listItems: [
                            {
                                text: 'не ранее 1950 года, этажностью не менее 4 этажей и иметь кирпичные наружные стены;'
                            }, {
                                text: 'не ранее 1950 года и этажностью не менее 6 этажей вне зависимости от материала стен;'
                            }, {
                                text: 'не ранее 1970 года и этажностью 5 этажей вне зависимости от материала стен.'
                            }
                        ],
                        ordered: true,
                    }, {
                        text: 'Для Санкт‑Петербурга:',
                        listItems: [
                            {
                                text: 'не ранее 1930 года <a href>постройки</a> и этажностью не менее 4‑х этажей вне зависимости от материала стен;'
                            }, {
                                text: 'не ранее 1800 года постройки вне зависимости от этажности и материала стен при наличии капитального ремонта, проведенного не ранее 1960 г. (в случае если капремонт проведен до 1960 г. дом должен быть с металлическим типом перекрытий).'
                            }
                        ],
                        ordered: true,
                    }, {
                        text: 'Для остальных регионов:',
                        listItems: [
                            {
                                text: 'не ранее 1950 года этажностью не менее 4 этажей и иметь кирпичные наружные стены;'
                            }, {
                                text: 'не ранее 1955 года и этажностью не менее 5 этажей вне зависимости от материала.'
                            }
                        ],
                        ordered: true,
                    }
                ]
            } }); }
    };
})
.add('Ordered list with nested unordered lists', () => {
    return {
        render(h) { return h(ListV1, { props: {
                ordered: true,
                items: [
                    {
                        text: 'Для всех регионов:',
                        listItems: [
                            {
                                text: 'не ранее 2000 года, этажностью не менее 3 этажей вне зависимости от материала стен.'
                            }
                        ]
                    }, {
                        text: 'Для Москвы и Московской области:',
                        listItems: [
                            {
                                text: 'не ранее 1950 года, этажностью не менее 4 этажей и иметь кирпичные наружные стены;'
                            }, {
                                text: 'не ранее 1950 года и этажностью не менее 6 этажей вне зависимости от материала стен;'
                            }, {
                                text: 'не ранее 1970 года и этажностью 5 этажей вне зависимости от материала стен.'
                            }
                        ]
                    }, {
                        text: 'Для Санкт‑Петербурга:',
                        listItems: [
                            {
                                text: 'не ранее 1930 года <a href>постройки</a> и этажностью не менее 4‑х этажей вне зависимости от материала стен;'
                            }, {
                                text: 'не ранее 1800 года постройки вне зависимости от этажности и материала стен при наличии капитального ремонта, проведенного не ранее 1960 г. (в случае если капремонт проведен до 1960 г. дом должен быть с металлическим типом перекрытий).'
                            }
                        ]
                    }, {
                        text: 'Для остальных регионов:',
                        listItems: [
                            {
                                text: 'не ранее 1950 года этажностью не менее 4 этажей и иметь кирпичные наружные стены;'
                            }, {
                                text: 'не ранее 1955 года и этажностью не менее 5 этажей вне зависимости от материала.'
                            }
                        ]
                    }
                ]
            } }); }
    };
})
.add('Unordered list with nested unordered lists', () => {
    return {
        render(h) { return h(ListV1, { props: {
                ordered: false,
                items: [
                    {
                        text: 'Для всех регионов:',
                        listItems: [
                            {
                                text: 'не ранее 2000 года, этажностью не менее 3 этажей вне зависимости от материала стен.'
                            }
                        ]
                    }, {
                        text: 'Для Москвы и Московской области:',
                        listItems: [
                            {
                                text: 'не ранее 1950 года, этажностью не менее 4 этажей и иметь кирпичные наружные стены;'
                            }, {
                                text: 'не ранее 1950 года и этажностью не менее 6 этажей вне зависимости от материала стен;'
                            }, {
                                text: 'не ранее 1970 года и этажностью 5 этажей вне зависимости от материала стен.'
                            }
                        ]
                    }, {
                        text: 'Для Санкт‑Петербурга:',
                        listItems: [
                            {
                                text: 'не ранее 1930 года <a href>постройки</a> и этажностью не менее 4‑х этажей вне зависимости от материала стен;'
                            }, {
                                text: 'не ранее 1800 года постройки вне зависимости от этажности и материала стен при наличии капитального ремонта, проведенного не ранее 1960 г. (в случае если капремонт проведен до 1960 г. дом должен быть с металлическим типом перекрытий).'
                            }
                        ]
                    }, {
                        text: 'Для остальных регионов:',
                        listItems: [
                            {
                                text: 'не ранее 1950 года этажностью не менее 4 этажей и иметь кирпичные наружные стены;'
                            }, {
                                text: 'не ранее 1955 года и этажностью не менее 5 этажей вне зависимости от материала.'
                            }
                        ]
                    }
                ]
            } }); }
    };
})
.add('Ordered list with nested ordered lists', () => {
    return {
        render(h) { return h(ListV1, { props: {
                ordered: true,
                items: [
                    {
                        text: 'Для всех регионов:',
                        listItems: [
                            {
                                text: 'не ранее 2000 года, этажностью не менее 3 этажей вне зависимости от материала стен.'
                            }
                        ],
                        ordered: true
                    }, {
                        text: 'Для Москвы и Московской области:',
                        listItems: [
                            {
                                text: 'не ранее 1950 года, этажностью не менее 4 этажей и иметь кирпичные наружные стены;'
                            }, {
                                text: 'не ранее 1950 года и этажностью не менее 6 этажей вне зависимости от материала стен;'
                            }, {
                                text: 'не ранее 1970 года и этажностью 5 этажей вне зависимости от материала стен.'
                            }
                        ],
                        ordered: true
                    }, {
                        text: 'Для Санкт‑Петербурга:',
                        listItems: [
                            {
                                text: 'не ранее 1930 года <a href>постройки</a> и этажностью не менее 4‑х этажей вне зависимости от материала стен;'
                            }, {
                                text: 'не ранее 1800 года постройки вне зависимости от этажности и материала стен при наличии капитального ремонта, проведенного не ранее 1960 г. (в случае если капремонт проведен до 1960 г. дом должен быть с металлическим типом перекрытий).'
                            }
                        ],
                        ordered: true
                    }, {
                        text: 'Для остальных регионов:',
                        listItems: [
                            {
                                text: 'не ранее 1950 года этажностью не менее 4 этажей и иметь кирпичные наружные стены;'
                            }, {
                                text: 'не ранее 1955 года и этажностью не менее 5 этажей вне зависимости от материала.'
                            }
                        ],
                        ordered: true
                    }
                ]
            } }); }
    };
});

storiesOf('Elements/Navigations', module)
.add('Navigation with social icons', () => ({
    render: h => h(IconsNavV1)
}))
.add('Navigation with store icons', () => ({
    render: h => h(IconsNavV1, { props: { links: [
                {  link: '', icon: 'store__icon store__apple' },
                {  link: '', icon: 'store__icon store__google' }
            ] } })
}))
.add('Navigation with icons and text', () => ({
    render: h => h(TextNavWithIconsV1, { props: {
            links: [
                { link: '', newTab: true, text: 'Инвестиционное и брокерское <span>обслуживание</span>', icon: 'icon__advantage1' },
                { link: '', newTab: false, text: 'Премиальные <span>карты</span>', icon: 'icon__advantage2' },
                { link: '', newTab: true, text: 'Финансовая <span>помощь</span> в поездках', icon: 'icon__advantage3' },
                { link: '', newTab: false, text: '<span>Страхование</span> жизни, недвижимости и финансов', icon: 'icon__advantage4' },
                { link: '', newTab: true, text: '<span>Персональный</span> менеджер и выделенные зоны обслуживания в отделениях', icon: 'icon__advantage5' },
                { link: '', newTab: false, text: 'Выгодные курсы <span>обмена валюты</span>', icon: 'icon__advantage6' }
            ]
        } })
}));

storiesOf('Elements/Stars', module)
.addDecorator(withKnobs)
.add('Stars', () => {
    return {
        props: {
            total: { default: number('Total', 5) },
            active: { default: number('Active', 2) }
        },
        render(h) { return h(StarsV1, { props: { total: this.total, active: this.active } }); }
    };
});

storiesOf('Elements/Icons', module)
.add('Burger', () => ({
    render: h => h(BurgerV1, { props: {} })
}))
.add('Back to top', () => ({
    render: h => h(BackToTopV1, { props: {} })
}));

storiesOf('Elements/PremiumDirect links', module)
.add('Premium/Direct links', () => ({
    render: h => h(PremiumDirectLinks, { props: {} })
}));

storiesOf('Elements/Switches', module)
.add('Switch', () => ({
    render: h => h(SwitchV1, { props: { name: 'Я зарпланый клиент банка' } })
}));

storiesOf('Elements/Range sliders', module)
.add('Range slider', () => ({
    render: h => h(RangeSliderV1, { props: { name: 'Слайдер в рублях' } })
}))
.add('Range slider with input', () => ({
    render: h => h(RangeSliderWithInputV1, { props: {
            name: 'Слайдер в рублях',
            info: '15%',
            // postfix: ['₽'],
            postfix: ['год', 'года', 'лет'],
            options: {
                max: 1000000,
            }
        } })
}));

storiesOf('Elements/Tables', module)
.add('Table', () => ({
    render: h => h(TableV1, { props: {
            rows: {
                head: [
                    [
                        {
                            text: 'Сумма на счете'
                        }, {
                        colspan: 2,
                        text: 'Годовые процентные ставки'
                    }
                    ]
                ],
                body: [
                    [
                        {
                            items: [
                                {
                                    text: ''
                                }
                            ]
                        }, {
                        items: [
                            {
                                text: 'для зарплатных клиентов'
                            }
                        ]
                    }, {
                        items: [
                            {
                                text: 'для не зарплатных клиентов'
                            }
                        ]
                    }
                    ], [
                        {
                            items: [
                                {
                                    text: 'До 100 тыс. ₽',
                                    class: 'bold'
                                }
                            ]
                        }, {
                            items: [
                                {
                                    text: '5,5%'
                                }
                            ]
                        }, {
                            items: [
                                {
                                    text: '4,5%'
                                }
                            ]
                        }
                    ], [
                        {
                            colspan: 3,
                            items: [
                                {
                                    text: 'От 13 месяцев до 5 лет',
                                    class: 'gray65'
                                }, {
                                    type: 'list',
                                    listItems: [
                                        { text: '250 тыс. ₽ для всех' },
                                        { text: 'До 1 млн <span class="gray65">(для других регионов)</span>' },
                                        { text: 'До 2 млн <span class="gray65">(для Москвы и Московской обл., Санкт-Петербурга и Ленинградской обл.)</span>' },
                                        { text: 'До 5 млн ₽ <span class="gray65">(для зарплатных клиентов, на срок кредита до 36 мес.)</span>' }
                                    ]
                                }, {
                                    text: 'Сумма кредита',
                                    class: 'gray65'
                                }, {
                                    type: 'list',
                                    listItems: [
                                        { text: '9,99% со второго года' },
                                        { text: '11,99% на первый год' }
                                    ],
                                    ordered: true
                                }
                            ]
                        }
                    ], [
                        {
                            items: [
                                {
                                    text: 'От 2 млн до 20 млн ₽',
                                    class: 'bold'
                                }
                            ]
                        }, {
                            items: [
                                {
                                    text: '4,0%'
                                }
                            ]
                        }, {
                            items: [
                                {
                                    text: '4,0%'
                                }
                            ]
                        }
                    ], [
                        {
                            items: [
                                {
                                    text: 'Свыше 20 млн ₽',
                                    class: 'bold'
                                }
                            ]
                        }, {
                            items: [
                                {
                                    text: '0,01%'
                                }
                            ]
                        }, {
                            items: [
                                {
                                    text: '0,01%'
                                }
                            ]
                        }
                    ]
                ]
            }
        } })
}));

storiesOf('Elements/Images', module)
.add('Image with download', () => ({
    render: h => h(ImageWithDownloadV1, { props: {
        img: './img/cardsTimeline.jpg',
        link: './img/cardsTimeline.jpg'
    } })
}));

storiesOf('Elements/Accordions', module)
.add('Accordion', () => ({
    render: h => h('div', { style: { backgroundColor: '#1a1a1a' } },
        [h(AccordionV1, { props: {
            items: [
                {
                    type: 'text',
                    content: ['Абзац текст 1', 'Абзац текст 2']
                }, {
                    type: 'list',
                    listItems: [
                        { text: 'Бесплатное обслуживание в рамках пакета Premium Banking' },
                        { text: 'Возможность открыть в трех валютах: рубли, доллары, евро' },
                    ]
                }, {
                    accordion: 'Список документов',
                    type: 'iconLink',
                    link: '',
                    newTab: false,
                    text: '<span>Условия</span> по дебетовой карте MasterCard World Black Edition Premium / Visa Platinum Premium',
                    icon: 'icon__file'
                }
            ]
        } })]
    )
}));

storiesOf('Elements/Cards', module)
.addDecorator(withKnobs)
.add('News card', () => {
    return {
        props: {
            big: { default: boolean('Увеличенная', false) },
            img: { default: text('Картинка', './img/offerImg1.jpg') },
            title: { default: text('Заголовок', 'Бесплатные интернет и мобильная связь в роуминге от «Мегафон»') },
            linkText: { default: text('Текст ссылки', 'Узнать больше') },
        },
        render(h) { return h(Card, {
            class: 'col',
            props: {
                big: this.big,
                img: this.img,
                title: this.title,
                linkText: this.linkText
            } }); }
    };
});
