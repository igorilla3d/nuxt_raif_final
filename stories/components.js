import { storiesOf } from "@storybook/vue";
import { boolean, text, number, withKnobs } from "@storybook/addon-knobs";
import store from '@/.storybook/store';

import StarsCalculatorV1 from '~/library/Components/Calculators/StarsCalculatorV1.vue';
import CurrenciesV1 from '~/library/Components/Currencies/CurrenciesV1.vue';
import Markdown from '~/library/Components/Markdown/Markdown.vue';
import News from '~/library/Components/News/NewsV1.vue';
import PageSliderV1 from '~/library/Components/PageSliders/PageSliderV1.vue';
import PageSliderV2 from '~/library/Components/PageSliders/PageSliderV2.vue';
import PageHeader from '~/library/Components/PageHeader/PageHeader.vue';
import PageFooter from '~/library/Components/PageFooter/PageFooter.vue';

storiesOf('Components', module)
.addDecorator(withKnobs)
.add('Stars calculator', () => ({
    render: h => h(StarsCalculatorV1, { props: {} })
}))
.add('Currencies block', () => ({
    render: h => h(CurrenciesV1, {
        props: {
            title1: ['300 тыс. — 6,5 млн ₽', 'от 300 тыс. до 6,5 млн ₽'],
            title2: ['от 6,5 млн ₽', 'от 6,5 млн ₽']
        }
    })
}))
.add('Markdown block', () => ({
    props: {
        knobHeading: { default: text('Заголовок', 'Пользуйтесь привилегиями бесплатно') },
        knobContent: { default: text('Контент', `### Получите бесплатное обслуживание при выполнении одного из условий.
- Или 2 500 000 ₽ на рублевых счетах и в инвестиционных продуктах на конец месяца.
- Или 250 000 ₽ трат по картам в месяц, в том числе по дополнительным и кредитным.
- Или 500 000 ₽ перевод заработной платы (от юридического лица).
- Или наберите 5 звезд, воспользовавшись калькулятором ниже:
        `) },
    },
    render(h) { return h(Markdown, {
        props: {
            heading: this.knobHeading,
            content: this.knobContent,
        }
    }); },
}))
.add('News block', () => ({
    render: h => h('div', { style: { padding: '0 15px', 'max-width': '1170px' } },
        [h(News, {
            class: 'offersPage__news',
            props: {
                items: [
                    {
                        big: true,
                        img: './img/offerImg1.jpg',
                        title: 'Бесплатные интернет и мобильная связь в роуминге от «Мегафон»',
                        link: 'oneOfferPage.html',
                        linkText: 'Узнать больше',
                        newTab: false
                    }, {
                        img: './img/offerImg2.jpg',
                        title: 'Скидка 20% на проезд по платным дорогам при пополнении транспондера «Главная Дорога»',
                        link: 'oneOfferPage.html',
                        newTab: false
                    }, {
                        img: './img/offerImg3.jpg',
                        title: 'Скидка 15% в Яндекс.Такси на тарифы «Бизнес», «Премиум» и «Ультима»',
                        link: 'oneOfferPage.html',
                        newTab: false
                    }, {
                        img: './img/offerImg4.jpg',
                        title: 'Бесплатный интернет и минуты в роуминге «Билайн», «МегаФон», МТС и Tele2',
                        link: 'oneOfferPage.html',
                        newTab: false
                    }, {
                        img: './img/offerImg5.jpg',
                        title: 'В 3 раза больше бонусов и бесплатная доставка в «Азбуке Вкуса»',
                        link: 'oneOfferPage.html',
                        newTab: false
                    }
                ]
            } })]
    )
}))
.add('Slider with fading content', () => ({
    store,
    render: h => h(PageSliderV1, { props: {
        slides: [
            {
                title: 'Вклад «Добро пожаловать»',
                text: 'Приумножайте ваши сбережения с повышенной процентной ставкой',
                textOnlyDesktop: true,
                bgImg: './img/insuranceBg1.jpg',
                overlayActive: false,
                overlayBtnText: 'Оформить продукт',
                shortItems: [
                    {
                        type: 'iconLinks',
                        links: [
                            {
                                link: '',
                                text: '<span class="tooltipWrap"><span class="tooltipBody">При оформлении с финансовой защитой, ставка на 1‑й год 11,99%</span>Ставка</span> 9,99% со второго года',
                                icon: 'icon__percent'
                            }, {
                                link: '',
                                text: 'Срок вклада — 1 год',
                                icon: 'icon__calendar'
                            }, {
                                link: '',
                                text: 'Минимальная сумма — 500 000 ₽',
                                icon: 'icon__money'
                            }
                        ]
                    }, {
                        type: 'buttons',
                        content: [
                            {
                                btn: 1,
                                name: 'Оформить продукт'
                            }, {
                                btn: 2,
                                name: 'Узнать больше'
                            }
                        ]
                    }
                ],
                items: [
                    {
                        type: 'text',
                        content: ['Повышенная процентная ставка для новых премиальных клиентов']
                    }, {
                        type: 'table',
                        rows: {
                            head: [
                                [
                                    {
                                        text: 'Сумма на счете'
                                    }, {
                                    colspan: 2,
                                    text: 'Годовые процентные ставки'
                                }
                                ]
                            ],
                            body: [
                                [
                                    {
                                        items: [
                                            {
                                                text: ''
                                            }
                                        ]
                                    }, {
                                        items: [
                                            {
                                                text: 'для <span class="tooltipWrap"><span class="tooltipBody">Программа действует на весь срок кредита</span>зарплатных</span> клиентов'
                                            }
                                        ]
                                    }, {
                                        items: [
                                            {
                                                text: 'для не зарплатных клиентов'
                                            }
                                        ]
                                    }
                                ], [
                                    {
                                        items: [
                                            {
                                                text: 'До 100 тыс. ₽',
                                                class: 'bold'
                                            }
                                        ]
                                    }, {
                                        items: [
                                            {
                                                text: '5,5%'
                                            }
                                        ]
                                    }, {
                                        items: [
                                            {
                                                text: '4,5%'
                                            }
                                        ]
                                    }
                                ], [
                                    {
                                        colspan: 3,
                                        items: [
                                            {
                                                text: 'От 13 месяцев до 5 лет',
                                                class: 'gray65'
                                            }, {
                                                type: 'list',
                                                listItems: [
                                                    { text: '250 тыс. ₽ для всех' },
                                                    { text: 'До 1 млн <span class="gray65">(для других регионов)</span>' },
                                                    { text: 'До 2 млн <span class="gray65">(для Москвы и Московской обл., <a href>Санкт-Петербурга</a> и Ленинградской обл.)</span>' },
                                                    { text: 'До 5 млн ₽ <span class="gray65">(для <span class="tooltipWrap"><span class="tooltipBody">Программа действует на весь срок <a href>кредита</a>, но не более чем на срок кредита до 36 мес. Учитывайте это при планировании.</span>зарплатных</span> клиентов, на срок кредита до 36 мес.)</span>' }
                                                ]
                                            }, {
                                                text: 'Сумма кредита',
                                                class: 'gray65'
                                            }, {
                                                type: 'list',
                                                listItems: [
                                                    { text: '9,99% со второго года' },
                                                    { text: '11,99% на первый год' }
                                                ],
                                                ordered: true
                                            }
                                        ]
                                    }
                                ], [
                                    {
                                        items: [
                                            {
                                                text: 'От 2 млн до 20 млн ₽',
                                                class: 'bold'
                                            }
                                        ]
                                    }, {
                                        items: [
                                            {
                                                text: '4,0%'
                                            }
                                        ]
                                    }, {
                                        items: [
                                            {
                                                text: '4,0%'
                                            }
                                        ]
                                    }
                                ], [
                                    {
                                        items: [
                                            {
                                                text: 'Свыше 20 млн ₽',
                                                class: 'bold'
                                            }
                                        ]
                                    }, {
                                        items: [
                                            {
                                                text: '0,01%'
                                            }
                                        ]
                                    }, {
                                        items: [
                                            {
                                                text: '0,01%'
                                            }
                                        ]
                                    }
                                ]
                            ]
                        }
                    }, {
                        type: 'text',
                        content: ['Вклад можно открыть не позднее месяца, следующего за месяцем открытия пакета услуг Premium Banking.'],
                        class: 'mini notice'
                    }
                ],
                accordions: [
                    {
                        name: 'Список документов',
                        items: [
                            {
                                type: 'iconLinks',
                                links: [
                                    {
                                        link: '',
                                        text: '<span>Условия</span> и процентные ставки по срочному вкладу (депозиту) «Добро пожаловать! (премиальный)»',
                                        icon: 'icon__file'
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }, {
                title: 'Накопительный счет «На каждый день»',
                text: 'Получайте доход каждый день. Проценты начисляются ежедневно',
                textOnlyDesktop: true,
                bgImg: './img/insuranceBg2.jpg',
                overlayActive: false,
                overlayBtnText: 'Оформить продукт',
                shortItems: [
                    {
                        type: 'iconLinks',
                        links: [
                            {
                                link: '',
                                text: 'Процентная ставка — до 5,5%',
                                icon: 'icon__percent'
                            }, {
                                link: '',
                                text: 'Максимальная <span class="tooltipWrap"><span class="tooltipBody">Сумма 5 млн руб. действует для зарплатных клиентов, на срок кредита до 36 мес.</span>сумма</span> до 5 млн ₽',
                                icon: 'icon__calendar'
                            }, {
                                link: '',
                                text: 'Пополнение и снятие средств без потери % ',
                                icon: 'icon__money'
                            }
                        ]
                    }, {
                        type: 'buttons',
                        content: [
                            {
                                btn: 1,
                                name: 'Оформить продукт'
                            }, {
                                btn: 2,
                                name: 'Узнать больше'
                            }
                        ]
                    }
                ],
                items: [
                    {
                        type: 'text',
                        content: ['Простое и легкое приумножение ваших средств. Процентная ставка по счету зависит от суммы на накопительном счете']
                    }, {
                        type: 'table',
                        rows: {
                            head: [
                                [
                                    {
                                        text: 'Сумма на счете'
                                    }, {
                                    colspan: 2,
                                    text: 'Годовые процентные ставки'
                                }
                                ]
                            ],
                            body: [
                                [
                                    {
                                        items: [
                                            {
                                                text: ''
                                            }
                                        ]
                                    }, {
                                        items: [
                                            {
                                                text: 'для зарплатных клиентов'
                                            }
                                        ]
                                    }, {
                                        items: [
                                            {
                                                text: 'для не зарплатных клиентов'
                                            }
                                        ]
                                    }
                                ], [
                                    {
                                        items: [
                                            {
                                                text: 'До 100 тыс. ₽',
                                                class: 'bold'
                                            }
                                        ]
                                    }, {
                                        items: [
                                            {
                                                text: '5,5%'
                                            }
                                        ]
                                    }, {
                                        items: [
                                            {
                                                text: '4,5%'
                                            }
                                        ]
                                    }
                                ], [
                                    {
                                        items: [
                                            {
                                                text: 'От 100 тыс. до 2 млн ₽',
                                                class: 'bold'
                                            }
                                        ]
                                    }, {
                                        items: [
                                            {
                                                text: '4,5%'
                                            }
                                        ]
                                    }, {
                                        items: [
                                            {
                                                text: '4,0%'
                                            }
                                        ]
                                    }
                                ], [
                                    {
                                        items: [
                                            {
                                                text: 'От 2 млн до 20 млн ₽',
                                                class: 'bold'
                                            }
                                        ]
                                    }, {
                                        items: [
                                            {
                                                text: '4,0%'
                                            }
                                        ]
                                    }, {
                                        items: [
                                            {
                                                text: '4,0%'
                                            }
                                        ]
                                    }
                                ], [
                                    {
                                        items: [
                                            {
                                                text: 'Свыше 20 млн ₽',
                                                class: 'bold'
                                            }
                                        ]
                                    }, {
                                        items: [
                                            {
                                                text: '0,01%'
                                            }
                                        ]
                                    }, {
                                        items: [
                                            {
                                                text: '0,01%'
                                            }
                                        ]
                                    }
                                ]
                            ]
                        }
                    }, {
                        type: 'text',
                        content: ['Ставка может комбинироваться. Например, если зарплатный клиент разместил на счет 300 тыс. ₽, то на сумму 100 тыс. ₽ начислят 5,5% годовых, а на остальные 200 тыс. ₽ — 4,5%.'],
                        class: 'mini notice'
                    }
                ],
                accordions: [
                    {
                        name: 'Условия',
                        items: [
                            {
                                type: 'list',
                                listItems: [
                                    { text: 'Открыть, пополнить и перевести средства со счета можно через интернет-банк и мобильное приложение в Райффайзен-Онлайн' },
                                    { text: 'Срок действия счета не ограничен' }
                                ]
                            }
                        ]
                    }, {
                        name: 'Список документов',
                        items: [
                            {
                                type: 'iconLinks',
                                links: [
                                    {
                                        link: '',
                                        text: 'Тарифы и процентные ставки',
                                        icon: 'icon__file'
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    } })
}))
.add('Slider with fading content and sliding cards', () => ({
    store,
    render: h => h(PageSliderV2, { props: {
        slides: [
            {
                overtitle: 'дебетовая карта',
                title: 'Mastercard World<br>Black Edition Premium',
                text: 'Удобная карта на каждый день — с открытием в 3 валютах и бонусами от партнеров',
                btn1Text: 'Оформить продукт',
                btn2Text: 'Узнать больше',
                bgImg: './img/insuranceBg3.jpg',
                cardImg: './img/card1.jpg',
                overlayActive: false,
                overlayBtnText: 'Оформить продукт',
                items: [
                    {
                        type: 'list',
                        listItems: [
                            { text: 'Бесплатное обслуживание в рамках пакета Premium Banking' },
                            { text: 'Возможность открыть в трех валютах: рубли, доллары, евро' },
                            { text: 'Снятие наличных за рубежом без комиссии' },
                            { text: 'Бесплатные уведомления об операциях и удобный <a href>Онлайн‑банк</a>' }
                        ]
                    }
                ],
                accordions: [
                    {
                        name: 'Привилегии по карте',
                        items: [
                            {
                                type: 'list',
                                listItems: [
                                    { text: 'Доступ в комфортабельный <a href>бизнес‑зал Mastercard</a> в Шереметьево' },
                                    { text: 'Свободный доступ в <a href>бизнес‑залы Mastercard Lounge</a> в Европе' },
                                    { text: '<a href>Скидка 10%</a> на экскурсии по всему миру Weatlas' },
                                    { text: '<a href>Скидка до 35%</a> и повышение класса автомобиля в Avis' },
                                    { text: '<a href>Скидка 10%</a> в ресторанах Ginza Project в Москве' },
                                    { text: '<a href>Скидка 10%</a> на билеты в «Синема Парк» и «Формула Кино»' }
                                ]
                            }
                        ]
                    }, {
                        name: 'Список документов',
                        items: [
                            {
                                type: 'iconLinks',
                                links: [
                                    {
                                        link: '',
                                        text: '<span>Условия</span> по дебетовой карте MasterCard World Black Edition Premium / Visa Platinum Premium',
                                        icon: 'icon__file'
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }, {
                overtitle: 'кредитная карта',
                title: 'Austrian Airlines<br>Masterсard World Black',
                text: 'Карта для полетов с Austrian Airlines, Lufthansa и авиакомпаниями Star Alliance',
                btn1Text: 'Оформить продукт',
                btn2Text: 'Узнать больше',
                bgImg: './img/insuranceBg4.jpg',
                cardImg: './img/card2.jpg',
                overlayActive: false,
                overlayBtnText: 'Оформить продукт',
                items: [
                    {
                        type: 'list',
                        listItems: [
                            { text: 'Бесплатное обслуживание в рамках пакета Premium Banking' },
                            { text: 'Возможность открыть в трех валютах: рубли, доллары, евро' },
                            { text: 'Снятие наличных за рубежом без комиссии' },
                            { text: 'Бесплатные уведомления об операциях и удобный <a href>Онлайн‑банк</a>' }
                        ]
                    }
                ],
                accordions: [
                    {
                        name: 'Привилегии по карте',
                        items: [
                            {
                                type: 'list',
                                listItems: [
                                    { text: '<a href>7 дней</a> — Бесплатный и безлимитный интернет в роуминге «Билайн», «МегаФон», МТС и Tele2' },
                                    { text: '<a href>Скидка 20%</a> на упаковку багажа с PACK&FLY и STAR BAG' },
                                    { text: '<a href>Скидки 10%</a> в ресторанах NOVIKOV GROUP в аэропорту «Шереметьево»' },
                                    { text: '<a href>Скидка 20%</a> на трансферы в аэропорт и обратно с Wheely и Gett' },
                                    { text: '<a href>Скидка 10%</a> в сети ресторанов White Rabbit Family' },
                                    { text: '<a href>Скидка 10%</a> на билеты и кинобар в сети кинотеатров «Каро»' }
                                ]
                            }
                        ]
                    }, {
                        name: 'Список документов',
                        items: [
                            {
                                type: 'iconLinks',
                                links: [
                                    {
                                        link: '',
                                        text: '<span>Условия</span> по дебетовой карте MasterCard World Black Edition Premium / Visa Platinum Premium',
                                        icon: 'icon__file'
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    } })
}))
.add('Page Header', () => ({
    store,
    render: h => h(PageHeader, { props: {} })
}))
.add('Page Footer', () => ({
    render: h => h(PageFooter, { props: {} })
}));
