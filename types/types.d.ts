interface IItem {
    type: string,
    [propName: string]: any
}

interface IAccordion {
    name: string,
    items: IItem[],
    info?: string
}

export interface ISlide {
    tag?: string,
    title: string,
    overtitle?: string,
    text?: string,
    textOnlyDesktop?: boolean,
    textHiddenOnMobile?: boolean,
    btn1Text?: string,
    btn2Text?: string,
    bgImg: string,
    cardImg: string,
    overlayActive: boolean,
    overlayBtnText?: string,
    items: IItem[],
    accordions?: IAccordion[]
}

export interface ISlide2 {
    tag?: string,
    title: string,
    text?: string,
    textOnlyDesktop?: boolean,
    textHiddenOnMobile?: boolean,
    bgImg: string,
    overlayActive: boolean,
    overlayBtnText?: string,
    shortItems: IItem[],
    items: IItem[],
    accordions?: IAccordion[]
}

export interface INews {
    title: string,
    link: string,
    linkText?: string,
    big?: boolean,
    img?: string,
    newTab?: boolean
}