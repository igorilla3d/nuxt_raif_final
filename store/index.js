export const state = () => ({
    activeSlideTag: '',
    topNavOpen: false,
});

export const actions = {
    setActiveSlideTag: ({ commit }, { tag }) => {
        commit('setActiveSlideTag', { tag: tag });
    },
    setTopNav: ({ commit }, { open }) => {
        commit('setTopNav', { open: open });
    }
};

export const mutations = {
    setActiveSlideTag: (state, { tag }) => {
        state.activeSlideTag = tag;
    },
    setTopNav: (state, { open }) => {
        state.topNavOpen = open;
    }
};
